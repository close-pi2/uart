from setuptools import find_packages, setup
from setuptools.command.sdist import sdist

setup(
    name='uart',
    version=__import__('cuart').__version__,
    install_requires=[
        'pybleno',
        'serial',
    ],
    packages=find_packages(),
    author='UnB',
    url='https://gitlab.com/close-pi2/uart',
    entry_points={
        'console_scripts': [
            'uart=uart.__main__:main',
        ],
    },
    cmdclass={
        'sdist': CustomSdistCommand,
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='Close'
)
