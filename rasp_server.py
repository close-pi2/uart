from zmq import *
import serial
import time

class SerialClose(object):

    def __init__(self):
        self.serialport = serial.Serial("/dev/serial0", baudrate=9600, timeout=3.0)
        self.serialport.flush()
        self.serialport.flushInput()
        self.serialport.flushOutput()
        self.context = Context()
        self.socket = self.context.socket(DEALER)
        self.socket.bind("tcp://*:5566")

    def forward(self):
        while True:
            write_bytes = self.serialport.read(3)
            print("Sent -> " + write_bytes)
            self.socket.send(write_bytes)
            message = self.socket.recv()
            print("Received -> " + message)

serial = SerialClose()
serial.forward()
